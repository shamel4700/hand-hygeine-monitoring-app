# Product Demonstration Report


**Software System Demonstration**

--Android Application—

![Android App](Final_Product_Documentation/Testing_Demos/Android_App/Android_Complete_Demo.mp4)

Shown above is a video demonstrating the standard operation of the Android application.

1. Upon first installing the application the user is prompted to allow storage writing and reading permissions. If the user refuses, the app will display a message and shut down as it cannot perform its intended function without storage access.

2. The app downloaded the online Excel file to the device in the background and indicates the user to wait while the data is read and processed.

3. The data is displayed in a table of time and date of recording, RFID and usage duration. 

4. The user may input an RFID in the search bar to display all entries of a specific RFID and a compliance rating based on those entries. The compliance rating displayed uses a duration of 30 seconds as the maximum threshold.

5. If no relevant entries are present a message is displayed.

6. Exiting the search function clears the search results and shows the original data table.

7. Pressing the refresh button clears the screen, and initiates a download of the latest version of the online Excel file, which is then processed and displayed as before. 

