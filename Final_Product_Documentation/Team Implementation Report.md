# Team Implementation Report


## Technical Description

**Android Application**  
The application was built in Android Studio using the Kotlin programming language. It is intended to run on Android devices from SDK version 23 till 30.

_Front End_  
The UI consists of a main LinearLayout (rootLayout), containing a TableRow, another LinearLayout (compliance) and a ScrollView. The TableRow contains three TextViews of equal width, and these represent the headings of the table of data to be displayed. The second LinearLayout is the space used for the TextView holding compliance rating information. The ScrollView contains a LinearLayout(mainLayout). mainLayout will be dynamically filled with TableRows of TextViews, to create a table of data obtained from an Excel file. The TextViews use black rectangular borders to simulate cells. 

The Action Bar contains two menu items(similar to buttons), which are for the search and refresh functions. These use default Android SVG icons.

_Back End_  
A single Activity class of the type AppCompatActivity was used. On every launch, the application checks whether it has storage and internet permissions using permCheck(). If it does, it calls allProcesses(), otherwise the overwritten method onRequestPermissionsResult() runs allProcesses() when permissions are granted. 
allProcesses() runs downloadFile() and registers the BroadCastReceiver mentioned below. In downloadFile, the app’s allocated storage is checked for the presence of the required Excel file. If present, it is deleted to simulate an overwrite. The new file is then queued for download using the DownloadManager class, while a BroadcastReceiver object is created and registered to notify the application when the download is completed. This is done by overriding the onReceive method of the BroadcastReceiver object. Once the download completes, onReceive will display a “Snackbar” message and run the function displayData().

displayData() uses functions from the library “android5xlsx by Andruhon” to read and process data from the downloaded Excel file. The first column is extracted as Dates while the other two are extracted as presented within the Excel file. The three values from each column are passed to another function createViews().

createViews() takes as parameters, three strings, and creates three TextViews from them. These views are encased in a new TableRow then added to mainLayout(see Section: Front End).

A search function called search() exists to obtain entries of a certain ID from the current set of data and also display their compliance rating. It takes as input a string (the ID) and processes the file in the same way as displayData(). It uses a serial search algorithm and calculates a compliance rating for each entry (where usage duration of 30 seconds and above yields a 100% rating). When the first result is found, the screen is cleared for the search results to be displayed. All results are displayed using createViews just as in displayData(). The compliance rating is calculated based on the average rating of each logged event and is displayed in a message in the compliance LinearLayout(see Section: Front End). If no results are found, a Snackbar message is displayed and the screen retains its current table of data. 

The method onCreateOptionsMenu() is overridden to set up the search and refresh buttons. The search button will open a SearchView into which a query can be typed. A setOnQueryTextListener is set up for this view to use search() once a query is submitted. A setOnCloseListener is used to set the actions that occur when the SearchView is closed using the “x” icon. Here it is meant to clear the screen of search results and revert to original data. A global Boolean flag “itemFound” is used to ensure that the screen only clears when a previous or current search yielded results. If that case occurs, the screen is cleared and displayData() is called to read from the file and display original data.

The method onOptionsItemSelected() is overridden to set the function of the refresh button. It will simply clear the screen and call allProcesses(). This will download the latest version of the Excel file, overwrite the previous one, and display its content. The button will disable for 5 seconds to prevent queuing multiple downloads at once, resulting in redundant downloaded files.


## Imported Libraries

**Apache POI**  
This library is a 3rd party collection of pure java libraries for reading and writing from Microsoft Office file formats such as .xlsx(Excel), which we have used for our cloud database. This is required when reading the excel file once downloaded.

**android5xlsx by andruhon**  
This library is a repack of an older version of the Apache POI library. It is needed to enable the reading and processing of Excel files in an Android application.
