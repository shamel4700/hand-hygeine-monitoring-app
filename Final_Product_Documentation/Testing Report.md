# Test Report

## Testing Performed

### Android App Testing

The following tests have been conducted to ensure that the mobile application has all expected functionality.

**Storage Permissions Test**

_Pass/Fail Condition_  
The application needs to read/write data from/to the device in order to function, thus asks the user for storage permissions. It must not run without these.

![Permission Test]( Final_Product_Documentation/Testing_Demos/Android_App/Permission_Test.mp4)

_Test Result_  
The application terminates if storage permissions are denied, as shown above, preventing any errors when trying to read/write from/to storage.


**Download Tests**

_Pass/Fail Condition_  
The application must download an Excel data file from Onedrive in the background on either WIFI or Mobile Data internet.

_Test Media_  
![WIFI Test]( Final_Product_Documentation/Testing_Demos/Android_App/WIFI_Test.mp4)

![Mobile Data Test]( Final_Product_Documentation/Testing_Demos/Android_App/Mobile_Data_Test.mp4)


_Test Result_  
The above videos demonstrate the application successfully downloading and displaying the Excel data on both WIFI and Mobile Data.


**Search RFID Test**

_Pass/Fail Condition_  
The application must be able to display entries for a certain RFID input by the user using the search option in the action bar. This function will either yield results if the RFID has exiting entries, or it will display a message indicating no results are present within the current set of data. 

_Test Media_  
![No Search Results]( Final_Product_Documentation/Testing_Demos/Android_App/Search_No_Result.mp4)

![Search with results]( Final_Product_Documentation/Testing_Demos/Android_App/Search_With_Result.mp4)


_Test Result_  
As shown by the set of videos, the application performs as expected, i.e. yielding results when present, or indicating otherwise when no entries for the requested RFID are present.



**Data Refresh Test**

_Pass/Fail Condition_  
The application implements a feature which allows the user to refresh the displayed data. This function downloads the latest version of the Excel file from OneDrive and displays it. The following video demonstrates a test on this feature. A search will be made for “12345”,when it is not present in the table. The online file will then be edited in the background to create an entry for “12345”. The application refresh feature will be used and the search will be made again.

_Test Media_  
![Data Refresh Test]( Final_Product_Documentation/Testing_Demos/Android_App/Data_Refresh.mp4)

_Test Result_  
The presented video shows that the data table shown in the application can indeed be synced to the latest version of the Excel file present in OneDrive.


