package com.team1.hygiene_recorder

import android.Manifest.permission
import android.app.DownloadManager
import android.app.SearchManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.SearchView
import android.widget.TableRow
import android.widget.TableRow.LayoutParams
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.widget.TextViewCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.snackbar.Snackbar
import com.team1.hygiene_recorder.databinding.ActivityMainBinding

import org.apache.poi.ss.usermodel.DataFormatter
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileInputStream
import java.util.*
import kotlin.concurrent.schedule


class MainActivity : AppCompatActivity() {
    private var permissionStatus: Int = 0 //used to check if permissions have been granted or not
    private var downloadID : Long = 0//download id of excel file
    lateinit var startMsg : Snackbar//message to tell user about background download and processing of file
    private var itemFound : Boolean = false//flag for setting whether table should be cleared or not depending on search results
    private lateinit var rootBinding: ActivityMainBinding

    private var bcReceiver = object: BroadcastReceiver(){//broastcast receiver for download
        override fun onReceive(context: Context, intent: Intent) {
            val bcDownloadID : Long = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            if(bcDownloadID == downloadID) {//if downloaded then display data
                startMsg.setText("Completed")
                displayData()
                Handler(Looper.getMainLooper()).postDelayed({//dismiss message after 1 second
                    startMsg.dismiss()
                }, 1000)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        permCheck()//check for storage permissions and ask if needed
        super.onCreate(savedInstanceState)
        rootBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        if (permissionStatus == 1) {//if permission already granted then proceed with app
            allProcesses()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(bcReceiver)
    }

    private fun permCheck() {//copied and modified from https://www.geeksforgeeks.org/android-how-to-request-permissions-in-android-application/
        when (PackageManager.PERMISSION_DENIED) {
            checkSelfPermission(
                    this,
                    permission.READ_EXTERNAL_STORAGE
            ) -> {//permission to read from storage
                requestPermissions(this, arrayOf(permission.READ_EXTERNAL_STORAGE), 1)

            }
            checkSelfPermission(
                    this,
                    permission.WRITE_EXTERNAL_STORAGE
            ) -> {//permission to write to storage
                requestPermissions(this, arrayOf(permission.WRITE_EXTERNAL_STORAGE), 1)
            }

            checkSelfPermission(this, permission.INTERNET) -> {//permission to use internet
                requestPermissions(this, arrayOf(permission.INTERNET), 1)
            }
            else -> {
                permissionStatus = 1
            }
        }
    }

    private fun allProcesses(){//que file for download and set a broadcast receiver for its completion notification
        downloadFile()
        val filter = IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        registerReceiver(bcReceiver, filter)
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {//written with guidance from https://www.geeksforgeeks.org/android-how-to-request-permissions-in-android-application/
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {//if permissions newly granted then do work
                    permissionStatus = 1
                    allProcesses()
                } else {
                    permissionStatus = 0
                    Snackbar.make(
                            rootBinding.rootLayout,
                            "This application cannot perform its intended function without storage permissions. Exiting soon...",
                            Snackbar.LENGTH_LONG
                    ).show()
                    Timer(
                            "ExitTimer",
                            false
                    ).schedule(4000) {//show on-screen message and exit app after 4 seconds
                        finish()
                    }
                }
            }
        }
    }

    private fun downloadFile() {// code written with guidance from two sources : https://stackoverflow.com/questions/63099515/how-to-download-file-from-url-in-android  AND https://www.youtube.com/watch?v=c-SDbITS_R4
        startMsg = Snackbar.make(rootBinding.rootLayout, "Please Wait...   (Keep Internet Enabled)", Snackbar.LENGTH_INDEFINITE)
        startMsg.show()
        try {
            val oldFile = File(getExternalFilesDirs(null)[0].absolutePath + "/DataFile.xlsx")//if file already exists then delete it
            if (oldFile.exists()) oldFile.delete()
            val dataURL = "https://onedrive.live.com/download?resid=DCBE98D5823317F9!34499&authkey=!ACMfz-NxGV5cUqM&ithint=file%2cxlsx&e=aw2MtX"
            val request = DownloadManager.Request(Uri.parse(dataURL))
            request.apply {
            setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI)
            setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE)
            setDestinationInExternalFilesDir(this@MainActivity, "", "DataFile.xlsx")
            setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
            setTitle("Updating Database")}
            val dlManager : DownloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            downloadID= dlManager.enqueue(request)//start download

        } catch (e: Exception) { //display exception messages
            println(e.toString())
        }
    }

    private fun createViews(time: String?, id: String?, duration: String?){//create rows with 3 fields
        val lParams = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0F)//set layout parameters
        val newRow = TableRow(this)//create row to hold textviews
        newRow.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

        val text1 = TextView(this)//create textview for time and date
        TextViewCompat.setTextAppearance(text1, R.style.textStyle_table_vals)//set textview properties
        text1.gravity = Gravity.CENTER
        text1.text = time
        text1.setBackgroundResource(R.drawable.border_rect)
        newRow.addView(text1, lParams)

        val text2 = TextView(this)//create textview for RFID
        TextViewCompat.setTextAppearance(text2, R.style.textStyle_table_vals)
        text2.gravity = Gravity.CENTER
        text2.text = id
        text2.setTextIsSelectable(true)
        text2.setBackgroundResource(R.drawable.border_rect)
        newRow.addView(text2, LayoutParams(0, LayoutParams.MATCH_PARENT, 1.0F))

        val text3 = TextView(this)//create textview for duration
        TextViewCompat.setTextAppearance(text3, R.style.textStyle_table_vals)
        text3.gravity = Gravity.CENTER
        text3.text = duration
        text3.setBackgroundResource(R.drawable.border_rect)
        newRow.addView(text3, LayoutParams(0, LayoutParams.MATCH_PARENT, 1.0F))
        rootBinding.mainLayout.addView(newRow)//add row to scroll layout
    }

    private fun displayData() {// written with guidance from https://stackoverflow.com/questions/46539934/read-content-of-xlsx-file-android
        try {
            val fileStream = (FileInputStream(File(getExternalFilesDirs(null)[0].absolutePath + "/DataFile.xlsx")))
            val workbook = XSSFWorkbook(fileStream) //create XSSF workbook from excel filestream
            val sheet: XSSFSheet = workbook.getSheetAt(1) //get second sheet in excel file
            val rowsCount: Int = sheet.physicalNumberOfRows //get total rows
            val rowArr = arrayOfNulls<String>(3) //create array to hold values from each row
            for (r in 7 until rowsCount) { //start from eighth row
                val row: Row = sheet.getRow(r)
                rowArr[0] = row.getCell(0).dateCellValue.toString()//get date and time formatted value
                for (c in 1..2) { //store each column value to array
                    val value = DataFormatter().formatCellValue(row.getCell(c)) //format data to be exactly as shown in excel file
                    rowArr[c] = value
                }
                createViews(rowArr[0], rowArr[1], rowArr[2]) // create table row
            }
            fileStream.close()
        } catch (e: java.lang.Exception) {
            println(e.toString())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {//create refresh button in menu and set search button function
        menuInflater.inflate(R.menu.mymenu, menu)


        val searchMng = getSystemService(Context.SEARCH_SERVICE) as SearchManager //search interface written with guidance from : https://www.youtube.com/watch?v=3aQgSsLkgJo
        val searchButton = menu?.findItem(R.id.search_button)
        val searchView = searchButton?.actionView as SearchView
        searchView.setSearchableInfo(searchMng.getSearchableInfo(componentName))

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            //set searchbox text listener
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    searchView.apply {
                        clearFocus()
                        setQuery("", false)
                    }
                    val flag = search(query)//search for input string
                    if (!itemFound) {
                        itemFound = flag
                    }
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {//if text changes do nothing
                return true
            }
        })
        searchView.setOnCloseListener(object : SearchView.OnCloseListener {
            override fun onClose(): Boolean {
                if (itemFound) {//if the search yieled a result, then closing the searchbar displays original data
                    rootBinding.mainLayout.removeAllViews()
                    rootBinding.compliance.removeAllViews()
                    rootBinding.invalidateAll()
                    displayData()
                    itemFound = false
                }
                searchView.onActionViewCollapsed()//collapse searchbar
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {//refresh button action
        if(item.itemId == R.id.refresh_button){//Refresh
            item.isEnabled = false //disable button for 5 seconds
            rootBinding.mainLayout.removeAllViews()//clear table and download file to display again
            rootBinding.compliance.removeAllViews()
            rootBinding.invalidateAll()
            allProcesses()
            Handler(Looper.getMainLooper()).postDelayed({
                item.isEnabled = true
            }, 5000)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun search(query: String) : Boolean{
        var found = 0
        try {
            var compliance = 0.0
            val unitCompliance = arrayListOf<Double>()
            val fileStream = (FileInputStream(File(getExternalFilesDirs(null)[0].absolutePath + "/DataFile.xlsx")))
            val workbook = XSSFWorkbook(fileStream) //create XSSF workbook from excel filestream
            val sheet: XSSFSheet = workbook.getSheetAt(1) //get second sheet in excel file
            val rowsCount: Int = sheet.physicalNumberOfRows //get total rows
            val rowArr = arrayOfNulls<String>(3) //create array to hold values from each row
            for (r in 7 until rowsCount) { //start from eighth row
                val row: Row = sheet.getRow(r)
                rowArr[0] = row.getCell(0).dateCellValue.toString()//get date and time formatted value
                for (c in 1..2) {
                    val value = DataFormatter().formatCellValue(row.getCell(c)) //format data to be exactly as shown in excel file
                    rowArr[c] = value
                }
                if(rowArr[1].equals(query)){
                    if(found == 0){//if value found then clear main table once
                        rootBinding.mainLayout.removeAllViews()
                        rootBinding.compliance.removeAllViews()
                        rootBinding.invalidateAll()
                    }
                    val duration = (rowArr[2]?.toInt() ?: 0)
                    unitCompliance.add(if (duration* 3.33 < 100) (duration * 3.33) else 100.0)
                    createViews(rowArr[0], rowArr[1], rowArr[2]) // create table row
                    found++
                }
            }
            fileStream.close()
            if(found == 0){
                Snackbar.make(rootBinding.rootLayout, "No Results Found.", Snackbar.LENGTH_LONG).show()
            }else{
                compliance = unitCompliance.sum()/found
                val complianceText = "The user with ID: $query has $found entries and a compliance rating of $compliance %."
                val textBox = TextView(this)//create textview for time
                TextViewCompat.setTextAppearance(textBox, R.style.textStyle_table_vals)//set textview properties
                textBox.gravity = Gravity.CENTER
                textBox.text = complianceText
                textBox.setPadding(25,5,25,5)

                textBox.setBackgroundResource(R.drawable.border_rect)
                rootBinding.compliance.addView(textBox,LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT))
            }
        } catch (e: java.lang.Exception) {
            Snackbar.make(rootBinding.rootLayout, e.toString(), Snackbar.LENGTH_LONG).show()//show exception
        }
        return found >0 //return true if search yielded results
    }
}


